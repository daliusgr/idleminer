#Idle Miner Tycoon clone#
  

##Unity version: 2018.2.2f1##
[Download unity version here](https://unity3d.com/get-unity/download?thank-you=update&download_nid=57864&os=Win)  
However, no advanced features were used, so should be compatible with other wersions.  

--

##Couple of notes on code architecture  

* Zenject plugin is used for dependency injection  
* Following on some things learned from ["Clean Code"](https://g.co/kgs/B8Rprn):  
	* Comments are kept to a minimum, compensated by descriptive class, function and variable names  
	* Functions are kept small and restricted to relatively constrained tasks  
* A kind of MVC pattern is used for this project:  
	* MonoBehaviours act as Views and give general acess to a game object  
	* Pure C# classes as controllers, factories and other bits to controll the views  
* Due to time constraints and minimal asset requirements, Resources and AssetBundles are not used (AssetBundles would be advisable otherwise)  
  
--
  
##Features  

* Multiple mines, that are configurable with ScriptableObject settings  
* Lift, warehouse and mineshaft facilities in each mine. These are also configured using ScriptableObject settings  
* Leveling up of Lifts, warehouses and mineshafts  
* Each mine can support 35 mineshafts, however there is no feature to add them in game, just in settings  
* Each mineshaft and warehouse will have multiple workers/miners when leveled up
* Wallet, displayed at the top
* Moving from one mine to another, without loosing progress
* Exiting the game does loose your progress, as saving and loading was not implemented. However backbone for it is there, since game parameters are serializable and can be saved as JSON